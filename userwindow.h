#ifndef USERWINDOW_H
#define USERWINDOW_H

#include <QMainWindow>
#include <QList>
#include "userinfo.h"

namespace Ui {
class UserWindow;
}

class WebcamMonitor;
class QListWidgetItem;


class UserWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit UserWindow(QWidget *parent = 0);
    ~UserWindow();

public slots:
    void SlotsActionExit();
    void SlotsActionAbout();
    void SlotsNewUser(UserInfo user);
    void SlotsItemDoubleClicked(QListWidgetItem * item);
    
private:
    Ui::UserWindow *ui;
    WebcamMonitor *monitor;
    QList<UserInfo> userlist;
};

#endif // USERWINDOW_H
