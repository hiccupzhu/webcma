/*
 * webcamconf.cpp
 *
 *  Created on: 2013-6-16
 *      Author: zhu
 */
#include <stdlib.h>
#include <string.h>
#include "webcamconf.h"
#include <QDebug>



#define err_exit(num,fmt,args...)  \
    do{printf("[%s:%d]"fmt"\n",__FILE__,__LINE__,##args);exit(num);} while(0)

#define err_return(num,fmt,args...)  \
    do{printf("[%s:%d]"fmt"\n",__FILE__,__LINE__,##args);return(num);} while(0)


UserInfo WebcamConf::userinfo;
lua_State* WebcamConf::L = NULL;

int WebcamConf::load_conf(const char* path)
{
    int ret = 0 ;
    if(L == NULL){
        L = luaL_newstate() ;         /* Create Lua state variable */
        if ( L == NULL ) err_exit(-1,"luaL_newstat() failed");
        luaL_openlibs(L);             /* Load Lua libraries */
    }
    ret = luaL_loadfile(L, path) ;      /* Load but don't run the Lua script */
    if ( ret != 0 ) {
        qDebug("luaL_loadfile failed ret=%d", ret) ;
        return ret;
    }
    ret = lua_pcall(L, 0, 0, 0) ;                 /* Run the loaded Lua script */
    if ( ret != 0 ) err_return(-1,"lua_pcall failed:%s",lua_tostring(L,-1)) ;

    lua_getglobal(L, "user");              //获取lua中定义的变量
    lua_getglobal(L, "ip");
    lua_getglobal(L, "sport");
    lua_getglobal(L, "tport");


    userinfo.set_user (lua_tostring(L, 1));
    userinfo.set_ip   (lua_tostring(L, 2));
    userinfo.set_sport(lua_tointeger(L, 3));
    userinfo.set_tport(lua_tointeger(L, 4));

    lua_pop(L, 4);

//    qDebug() << "###1:" << userinfo->marshal();
//    UserInfo info = userinfo->unmarshal(userinfo->marshal());
//    qDebug() << "###2:" << info.marshal();


    return 0;
}
