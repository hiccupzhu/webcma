#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "webcamconf.h"

#include <QDebug>
#include <QMessageBox>
#include <QString>

#include "gdef.h"

#define WEBCAM_SEND "webcam_send"
#define WEBCAM_RCV  "webcam_rcv"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    int ret = 0;
    inst_send = NULL;
    inst_rcv = NULL;
    vlcPlayer = NULL;

    ui->setupUi(this);
    setWindowFlags(windowFlags()& ~Qt::WindowMaximizeButtonHint);
    setFixedSize(this->width(), this->height());

    connect(ui->actionStart, SIGNAL(triggered()), this, SLOT(SlotsActionStart()));
    connect(ui->actionExit,  SIGNAL(triggered()), this, SLOT(SlotsActionExit()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(SlotsActionAbout()));


    if((inst_send = libvlc_new(0,NULL)) == NULL) {
        qDebug("Could not init inst_send libVLC");
        exit(1);
    }

    if((inst_rcv = libvlc_new(0,NULL)) == NULL) {
        qDebug("Could not init inst_rcv libVLC");
        exit(1);
    }
    qDebug() << "libVLC version: " << libvlc_get_version();



}

MainWindow::~MainWindow()
{
    SlotsActionStop();
    if(inst_send){
        libvlc_release(inst_send);
        inst_send = NULL;
    }
    if(inst_rcv){
        libvlc_release(inst_rcv);
        inst_rcv = NULL;
    }
    delete ui;
}

void MainWindow::SlotsActionStart()
{
#define qtu( i ) ((i).toUtf8().constData())

    if(ui->actionStart->text() == "停止"){
        SlotsActionStop();
        return ;
    }

    /*
    "dshow:// :dshow-size=640*480 :dshow-fps=12 :live-caching=0 :network-caching=0 :sout-mux-caching=0  \
    :sout=\"#transcode{venc=x264{profile=high,keyint=25,min-keyint=5,scenecut=40,opengop=false,level=41,\
    pass=1,me=hex,merange=16,preset=medium,tune=zerolatency},\
    vcodec=h264,vb=320,scale=1,acodec=mpga,ab=128,channels=2,samplerate=44100}\
    :std{access=udp,mux=ts,caching=0,dst=127.0.0.1:20000}\"";
    */

    const char* params_send[] = {
            "dshow-size=640*480",
            "dshow-fps=12",
            "live-caching=0",
            "network-caching=0",
            "sout-mux-caching=0"};

    libvlc_vlm_add_broadcast(inst_send,
            WEBCAM_SEND,
            "dshow://",
            "#transcode{venc=x264{profile=high,keyint=25,min-keyint=5,scenecut=40,\
            opengop=false,level=41,pass=1,me=hex,merange=16,preset=medium,tune=zerolatency},\
            vcodec=h264,vb=320,scale=1,acodec=mpga,ab=128,channels=2,samplerate=44100}\
            :std{access=udp,mux=ts,caching=0,dst=127.0.0.1:20000}",
            5, params_send, // <= 5 == sizeof(param) == count of parameters
            1, 0);


    libvlc_media_t *m = libvlc_media_new_location( inst_rcv, "udp://@:20000" );
    if( !m ) return ;
    libvlc_media_add_option(m, "sout-display-delay=0");

    vlcPlayer = libvlc_media_player_new_from_media (m);
    libvlc_media_release(m);

    libvlc_media_player_set_hwnd(vlcPlayer, (void*)ui->videoWidget->winId());


    libvlc_media_player_play (vlcPlayer);
    libvlc_vlm_play_media(inst_send, WEBCAM_SEND);
//    libvlc_vlm_play_media(inst_rcv, WEBCAM_RCV);

    ui->actionStart->setText("停止");

}

void MainWindow::SlotsActionStop()
{
    if(ui->actionStart->text() == "停止"){
        libvlc_vlm_stop_media(inst_send, WEBCAM_SEND);
        if(vlcPlayer) {
                libvlc_media_player_stop(vlcPlayer);
                libvlc_media_player_release(vlcPlayer);
        }
        vlcPlayer = NULL;

        ui->actionStart->setText("启动");
    }
}

void MainWindow::SlotsActionExit()
{
    SlotsActionStop();
    exit(0);
}

void MainWindow::SlotsActionAbout()
{
    QMessageBox::information(this, "关于",
                             "Webcam 版本: 0.1.0\n"
                             "当前版本仅为测试版本,如有需要可在以后另作修正.\n"
                             "                     \nCopyright Reserved",
                             QMessageBox::Accepted, QMessageBox::Accepted);
}
