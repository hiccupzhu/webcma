/*
 * webcamconf.h
 *
 *  Created on: 2013-6-16
 *      Author: zhu
 */

#ifndef WEBCAMCONF_H_
#define WEBCAMCONF_H_

#ifdef __cplusplus
extern "C"{
#endif

#include <stdint.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include "gdef.h"

#ifdef __cplusplus
}
#endif

#include "userinfo.h"

class WebcamConf{
public:
    static int load_conf(const char* path);

    static UserInfo get_userinfo(){ return userinfo; }
    static UserInfo userinfo;

private:
    static lua_State * L;
};


#endif /* WEBCAMCONF_H_ */
