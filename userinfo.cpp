#include "userinfo.h"
#include <QDebug>
#include <QString>
#include <QStringList>
#include <string.h>

UserInfo::UserInfo(){
    reset();
}

UserInfo::~UserInfo(){
    reset();
}

void UserInfo::reset(){
    sport = 0;
    tport = 0;
    connected = false;
}

void UserInfo::set_user(QString _user){
    username = _user;
}

void UserInfo::set_ip(QString _ip){
    ip = _ip;

}

void UserInfo::set_sport(const uint16_t v)
{
    sport = v;
}

void UserInfo::set_tport(const uint16_t v)
{
    tport = v;
}

void UserInfo::set_connected(const bool on)
{
    connected = on;
}

QString UserInfo::marshal()
{

    QString info;
    info.sprintf("user=%s|ip=%s|sport=%d|tport=%d|connected=%s",
            username.toLocal8Bit().data(), ip.toLocal8Bit().data(), sport, tport, connected ? "true" : "false");

//    qDebug() << info;
    return info;
}

UserInfo UserInfo::unmarshal(QString packet)
{
    UserInfo info;
    QStringList packets = packet.split("|");
    QList<QString>::Iterator it = packets.begin(),itend = packets.end();

    for(;it != itend; it ++){
        QStringList items = (*it).split("=");
        if(items[0] == "user"){
//            qDebug() << "items[1]=" << items[1];
            info.set_user(items[1]);
        }else if(items[0] == "ip"){
            info.set_ip(items[1]);
        }else if(items[0] == "sport"){
            info.set_sport(items[1].toInt());
        }else if(items[0] == "tport"){
            info.set_tport(items[1].toInt());
        }else if(items[0] == "connected"){
            if(items[1] == "true")
                info.set_connected(true);
            else
                info.set_connected(false);
        }else{
            qDebug("unmarshal() except!");
        }
    }

    return info;
}
