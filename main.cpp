#include "userwindow.h"
#include <QApplication>
#include <QTextCodec>
#include "webcammonitor.h"
#include "webcamconf.h"

int main(int argc, char *argv[])
{
    int ret;

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    QApplication a(argc, argv);

    ret = WebcamConf::load_conf("./webcam.lua");
    if(ret < 0){
        qDebug("open config file FAILED!!!\n");
    }


    UserWindow w;
    w.show();


    
    return a.exec();
}
