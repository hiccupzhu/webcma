#ifndef USERINFO_H
#define USERINFO_H

#include <stdint.h>
#include "gdef.h"
#include <QString>


class UserInfo{
public:
    UserInfo();
    ~UserInfo();

    void reset();
    void set_user(QString _user);
    void set_ip(QString _ip);
    void set_sport(const uint16_t v);
    void set_tport(const uint16_t v);
    void set_connected(const bool on);

    QString marshal();
    static UserInfo unmarshal(QString info);

    QString username;
    QString ip;
    uint16_t sport;
    uint16_t tport;
    bool  connected;
};

#endif // USERINFO_H
