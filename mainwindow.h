#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <vlc/vlc.h>

class WebcamConf;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    

public slots:
    void SlotsActionStart();
    void SlotsActionStop();
    void SlotsActionExit();
    void SlotsActionAbout();

private:
    Ui::MainWindow *ui;

    libvlc_instance_t     *inst_send;
    libvlc_instance_t     *inst_rcv;
    libvlc_media_player_t *vlcPlayer;
};

#endif // MAINWINDOW_H
