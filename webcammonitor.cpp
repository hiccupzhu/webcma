#include "webcammonitor.h"

#include <QUdpSocket>
#include <QHostAddress>
#include <unistd.h>
#include <QThread>
#include <QNetworkInterface>
#include <QList>
#include <QByteArray>
#include "gdef.h"
#include "webcamconf.h"

#define MONITOR_PORT 20000
#define MONITOR_ADDR "234.0.0.100"

WebcamMonitor::WebcamMonitor(QObject *parent):
    QObject(parent),
    quit(false)
{
    udp_socket = new QUdpSocket();
    if(!udp_socket){ qDebug("QUdpSocket new FIALED!!!"); exit(1);}

    if(!udp_socket->bind(QHostAddress::AnyIPv4, MONITOR_PORT, QUdpSocket::ShareAddress)){
        qDebug("QUdpSocket bind FIALED!!!");
        exit(1);
    }

//    QList<QHostAddress> nets = QNetworkInterface::allAddresses();
//    foreach(QHostAddress address, nets){
//        qDebug("%s", address.toString().toUtf8().data());
//    }

    QHostAddress mcast_addr(MONITOR_ADDR);
//    udp_socket->setSocketOption(QAbstractSocket::MulticastLoopbackOption, 0);//禁止本机接收
    if(!udp_socket->joinMulticastGroup(mcast_addr)){
        av_print("joinMulticastGroup() FAILED!!!");
//        exit(1);
    }
    connect(udp_socket, SIGNAL(readyRead()), this, SLOT(dataReady()));

//    pthread_create(&multicase_reader, NULL,  multicase_reader_thread_func_ext, this);
    pthread_create(&multicase_writer, NULL,  multicase_writer_thread_func_ext, this);
//
//    pthread_t mon_init;
//    pthread_create(&mon_init, NULL, WebcamMonitor_init_ext, this);


}

WebcamMonitor::~WebcamMonitor()
{
    SAFE_DELETE(udp_socket);
}

void*  WebcamMonitor::WebcamMonitor_init()
{


    return NULL;
}

void WebcamMonitor::dataReady()
{
#if 1
    int num;
    static const int MAX_BUF = 1500;
    static char buf[MAX_BUF];

    num = udp_socket->readDatagram(buf, MAX_BUF);
//    qDebug("RCV[%d] %s", num, buf);

    emit signalNewUser(UserInfo::unmarshal(buf));
#else
    QByteArray datagram;
    datagram.resize(udp_socket->pendingDatagramSize());
    udp_socket->readDatagram(datagram.data(), datagram.size());
    qDebug() << (tr("Received datagram: \"%1\"").arg(datagram.data()));
#endif


}

void *WebcamMonitor::multicase_reader_thread_func()
{
    const int MAX_BUF = 1500;
    int num;
    char *buf = (char*)malloc(MAX_BUF);
    while(!quit){
        memset(buf, 0, MAX_BUF);
        num = udp_socket->readDatagram(buf, MAX_BUF, 0, 0);
        qDebug("RCV[%d] %s", num, buf);
    }

    SAFE_FREE(buf);

    return NULL;
}

void* WebcamMonitor::multicase_writer_thread_func()
{
    while(!quit){
        QHostAddress mcast_addr(MONITOR_ADDR);//组播地址与服务器相同
        QUdpSocket udp_socket;
        QString info = WebcamConf::get_userinfo().marshal();
        udp_socket.writeDatagram(info.toLocal8Bit().data(), info.toLocal8Bit().size(), mcast_addr, MONITOR_PORT);//向服务器发送数据（UDP_SEND_PORT与服务器的监听端口相同）
        QThread::sleep(2);
    }
    return NULL;
}
