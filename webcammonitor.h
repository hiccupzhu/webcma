#ifndef WEBCAMMONITOR_H
#define WEBCAMMONITOR_H

#include <stdint.h>
#include <pthread.h>
#include <qobject.h>
#include "userinfo.h"

class QUdpSocket;

class WebcamMonitor : public QObject
{
    Q_OBJECT
public:
    WebcamMonitor(QObject *parent = 0);
    virtual ~WebcamMonitor();

    static void* WebcamMonitor_init_ext(void* args){
        WebcamMonitor* mon =(WebcamMonitor*)args;
        return mon->WebcamMonitor_init();
    }

    static void*  multicase_reader_thread_func_ext(void* args){
        WebcamMonitor* mon =(WebcamMonitor*)args;
        return mon->multicase_reader_thread_func();
    }
    static void*  multicase_writer_thread_func_ext(void* args){
        WebcamMonitor* mon =(WebcamMonitor*)args;
        return mon->multicase_writer_thread_func();
    }
    void*  WebcamMonitor_init();
    void*  multicase_reader_thread_func();
    void*  multicase_writer_thread_func();


    bool quit;

signals:
    void signalNewUser(UserInfo user);

private slots:
    void dataReady();

private:
    pthread_t  multicase_reader;
    pthread_t  multicase_writer;
    QUdpSocket *udp_socket;
};

#endif // WEBCAMMONITOR_H
