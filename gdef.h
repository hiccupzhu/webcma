/*
 * gdef.h
 *
 *  Created on: 2013-6-16
 *      Author: zhu
 */

#ifndef GDEF_H_
#define GDEF_H_


#ifndef MMIN
#define MMIN(a,b) ((a)>(b)?(b):(a))
#endif

#ifndef MMAX
#define MMAX(a,b) ((a)>(b)?(a):(b))
#endif

#ifndef MMID
#define MMID(a,m,b) (MMIN(MMAX(a,m),b))
#endif

#ifndef SAFE_FREE
#define SAFE_FREE(x) \
        do{\
            if((x) != NULL){\
                free((x));\
                (x) = NULL;\
            }\
        }while(0)
#endif

#ifndef SAFE_CLOSE
#define SAFE_CLOSE(x) \
        do{\
            if((x) != -1){\
                close((x));\
                (x) = -1;\
            }\
        }while(0)
#endif

#ifndef SAFE_FCLOSE
#define SAFE_FCLOSE(x) \
        do{\
            if((x) != NULL){\
                fclose((x));\
                (x) = NULL;\
            }\
        }while(0)
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(x) \
        do{\
            if((x) != NULL){\
                delete((x));\
                (x) = NULL;\
            }\
        }while(0)
#endif

#ifndef av_print
#define av_print(fmt, ...) \
    do{\
        qDebug("[%s::%s::%d]" fmt, __FILE__, __func__, __LINE__, ##__VA_ARGS__);\
    }while(0)
#endif

#endif /* GDEF_H_ */
