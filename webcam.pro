#-------------------------------------------------
#
# Project created by QtCreator 2013-06-16T01:02:48
#
#-------------------------------------------------

QT += core gui
QT += widgets
QT += network

TARGET = webcam
TEMPLATE = app

#INCLUDEPATH += /mingw/include
LIBS += -llua

CONFIG += console
CONFIG += link_pkgconfig
PKGCONFIG += libvlc

SOURCES += main.cpp\
        mainwindow.cpp \
    webcamconf.cpp \
    webcammonitor.cpp \
    userinfo.cpp \
    userwindow.cpp

HEADERS  += mainwindow.h \
    webcamconf.h \
    webcammonitor.h \
    userinfo.h \
    userwindow.h

FORMS    += mainwindow.ui \
    userwindow.ui

RESOURCES += \
    webcam.qrc

RC_FILE = webcam.rc

OTHER_FILES += \
    webcam.rc
