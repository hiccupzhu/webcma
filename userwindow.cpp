#include "userwindow.h"
#include "ui_userwindow.h"
#include <QDebug>
#include <QMessageBox>
#include "webcammonitor.h"
#include <QIcon>
#include<QListWidget>
#include<QListWidgetItem>

UserWindow::UserWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::UserWindow)
{
    monitor = new WebcamMonitor(this);

    ui->setupUi(this);
    setWindowFlags(windowFlags()& ~Qt::WindowMaximizeButtonHint);
    setFixedSize(this->width(), this->height());

    connect(ui->actionExit,  SIGNAL(triggered()), this, SLOT(SlotsActionExit()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(SlotsActionAbout()));
    connect(monitor, SIGNAL(signalNewUser(UserInfo)), this, SLOT(SlotsNewUser(UserInfo)));

    connect(ui->listUsers, SIGNAL(itemDoubleClicked(QListWidgetItem *)),\
            this, SLOT(SlotsItemDoubleClicked(QListWidgetItem *)));

    ui->listUsers->setIconSize(QSize(37, 37));

//    QListWidgetItem *item1 = new QListWidgetItem(QIcon(":/ico/username.ico"), "Oak", ui->listUsers);
//    QListWidgetItem *item2 = new QListWidgetItem(QIcon(":/ico/username.ico"), "Banana11", ui->listUsers);
//
//    ui->listUsers->insertItem(1, item1);
//    ui->listUsers->insertItem(2, item2);

}

UserWindow::~UserWindow()
{
    delete ui;
}

void UserWindow::SlotsNewUser(UserInfo user)
{
//    qDebug() << "##:" << user.marshal();

    QList<QListWidgetItem *> items = ui->listUsers->findItems(user.username, Qt::MatchFixedString);

    if(items.size() == 0){
        QListWidgetItem *item = new QListWidgetItem(QIcon(":/ico/user2.jpg"), user.username, ui->listUsers);
        ui->listUsers->addItem(item);
        userlist.append(user);
    }


}

void UserWindow::SlotsItemDoubleClicked(QListWidgetItem * item)
{
    UserInfo u;
    foreach(u, userlist){
        if(u.username == item->text()){
            break;
        }
    }
    qDebug() << "item:" << u.marshal();
}












void UserWindow::SlotsActionExit()
{
    exit(0);
}

void UserWindow::SlotsActionAbout()
{
    QMessageBox::information(this, "关于",
                             "Webcam 版本: 0.1.0\n"
                             "当前版本仅为测试版本,如有需要可在以后另作修正.\n"
                             "                     \nCopyright Reserved",
                             QMessageBox::Accepted, QMessageBox::Accepted);
}

